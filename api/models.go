package api

import ()

type Payment struct {
	Type           string
	Id             string
	Version        int
	OrganisationId string `json:"organisation_id"`
	Attributes     Attributes
}

type Attributes struct {
	Amount               string
	BeneficiaryParty     BeneficiaryParty   `json:"beneficiary_party"`
	ChargesInformation   ChargesInformation `json:"charges_information"`
	Currency             string
	DebtorParty          DebtorParty `json:"debtor_party"`
	EndToEndReference    string      `json:"end_to_end_reference"`
	Fx                   Fx
	NumericReference     string `json:"numeric_reference"`
	PaymentId            string `json:"payment_id"`
	PaymentPurpose       string `json:"payment_purpose"`
	PaymentScheme        string `json:"payment_scheme"`
	PaymentType          string `json:"payment_type"`
	ProcessingDate       string `json:"processing_date"`
	Reference            string
	SchemePaymentSubType string       `json:"scheme_payment_sub_type"`
	SchemePaymentType    string       `json:"scheme_payment_type"`
	SponsorParty         SponsorParty `json:"sponsor_party"`
}

type BeneficiaryParty struct {
	AccountName       string `json:"account_name"`
	AccountNumber     string `json:"account_number"`
	AccountNumberCode string `json:"account_number_code"`
	AccountType       int    `json:"account_type"`
	Address           string
	BankId            string `json:"bank_id"`
	BankIdCode        string `json:"bank_id_code"`
	Name              string
}

type SenderCharge struct {
	Amount   string
	Currency string
}

type ChargesInformation struct {
	BearerCode              string         `json:"bearer_code"`
	SenderCharges           []SenderCharge `json:"sender_charges"`
	ReceiverChargesAmount   string         `json:"receiver_charges_amount"`
	ReceiverChargesCurrency string         `json:"receiver_charges_currency"`
}

type DebtorParty struct {
	AccountName       string `json:"account_name"`
	AccountNumber     string `json:"account_number"`
	AccountNumberCode string `json:"account_number_code"`
	Address           string
	BankId            string `json:"bank_id"`
	BankIdCode        string `json:"bank_id_code"`
	Name              string
}

type Fx struct {
	ContractReference string `json:"contract_reference"`
	ExchangeRate      string `json:"exchange_rate"`
	OriginalAmount    string `json:"original_amount"`
	OriginalCurrency  string `json:"original_currency"`
}

type SponsorParty struct {
	AccountNumber string `json:"account_number"`
	BankId        string `json:"bank_id"`
	BankIdCode    string `json:"bank_id_code"`
}
