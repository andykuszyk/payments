#!/bin/bash

# Check for dependencies
if [[ "$(which jq)" == "" ]]; then
    echo "This test requires jq to be installed."
    exit 1
fi

# Deploy API.
docker stack deploy -c docker-compose.yml payments
sleep 10

function end_test() {
    exit_code=$1
    docker stack rm payments
    exit $exit_code
}

# First, POST a new payment.
post_response=$(curl localhost:8080/payments -d @testdata/payment.json -w STATUS:%{http_code} -v)
post_status=$(echo $post_response | sed 's/.*STATUS://g')
echo "POST /payments returned a $post_status"
if [[ "$post_status" != "201" ]]; then
    echo "Test failed, because POST /payments should have returned a 201"
    end_test 1
fi
post_json=$(echo $post_response | sed 's/STATUS:.*//g')
payment_id=$(echo $post_json | jq .Id -r)

# Then, try GET-ing the payment we just posted.
get_response=$(curl localhost:8080/payments/$payment_id -w STATUS:%{http_code} -v)
get_status=$(echo $get_response | sed 's/.*STATUS://g')
echo "GET /payments/$payment_id returned a $get_status"
if [[ "$get_status" != "200" ]]; then
    echo "Test failed, because GET /payment/$payment_id should have returned a 200"
    end_test 1
fi
if [[ "$post_json" != "$(echo $get_response | sed 's/STATUS:.*//g')" ]]; then
    echo "Test failed, because the POST response and the GET response were not equal"
    end_test 1
fi

# Now, update the payment.
put_request=$(echo $post_json | sed 's/"organisation_id":"[a-z0-9\-]*"/"organisation_id":"spam-eggs"/g')
echo $put_request > put_request
put_response=$(curl -X PUT localhost:8080/payments/$payment_id -d @put_request -v -w STATUS:%{http_code})
rm put_request
put_status=$(echo $put_response | sed 's/.*STATUS://g')
echo "PUT /payments/$payment_id returned a $put_status"
if [[ "$put_status" != "200" ]]; then
    echo "Test failed, because PUT /payments/$payment_id should have returned a 200"
    end_test 1
fi

# Try fetching the payment by its new organisation id.
get_response=$(curl localhost:8080/payments?organisationId=spam-eggs -v -w STATUS:%{http_code})
get_status=$(echo $get_response | sed 's/.*STATUS://g')
echo "GET /payments?organisationId=spam-eggs returned a $get_status"
if [[ "$get_status" != "200" ]]; then
    echo "Test failed, because GET /payment?organisationId=spam-eggs should have returned a 200"
    end_test 1
fi

# Finally, delete the payment and check that we get a 404 afterwards.
delete_response=$(curl -X DELETE localhost:8080/payments/$payment_id -v -w STATUS:%{http_code})
delete_status=$(echo $delete_response | sed 's/.*STATUS://g')
echo "DELETE /payments/$paymend_id returned a $delete_status"
if [[ "$delete_status" != "200" ]]; then
    echo "Test failed, because DELETE /payments/$payment_id should have returned a 200"
    end_test 1
fi
get_response=$(curl localhost:8080/payments/$payment_id -w STATUS:%{http_code} -v)
get_status=$(echo $get_response | sed 's/.*STATUS://g')
echo "GET /payments/$payment_id returned a $get_status"
if [[ "$get_status" != "404" ]]; then
    echo "Test failed, because GET /payment/$payment_id should have returned a 404, following a DELETE"
    end_test 1
fi

echo "Test passed!"

# Teardown the API.
end_test 0
