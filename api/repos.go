package api

import (
	"context"
	"log"
	"os"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Defines the methodset for a repository of Payments, supporting
// crud operations.
type PaymentsRepo interface {
	Insert(payment Payment) (string, error)
	FetchById(id string) (*Payment, error)
	FetchByOrganisationId(id string) ([]Payment, error)
	Delete(id string) error
	Update(payment Payment) error
}

// Implementation of the `PaymentsRepo`, allowing payments
// to be saved to and retrieved from a mongo db.
type MongoPaymentsRepo struct {
	ctx        context.Context
	client     *mongo.Client
	collection *mongo.Collection
}

// Instantiates a new repo with an active connection to the mongodb database.
func NewMongoPaymentsRepo() (*MongoPaymentsRepo, error) {
	var mongoAddr string
	if os.Getenv("MONGO_ADDR") == "" {
		mongoAddr = "mongodb://localhost:27017"
	} else {
		mongoAddr = os.Getenv("MONGO_ADDR")
	}
	log.Printf("Connecting to mongo db at: %s", mongoAddr)
	ctx := context.Background()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoAddr))
	collection := client.Database("payments").Collection("payments")
	if err != nil {
		return nil, err
	}
	return &MongoPaymentsRepo{
		client:     client,
		ctx:        ctx,
		collection: collection,
	}, nil
}

// Inserts a `Payment` into the database, assigning it a new id, which is
// returned as a string.
func (p *MongoPaymentsRepo) Insert(payment Payment) (string, error) {
	payment.Id = uuid.New().String()
	_, err := p.collection.InsertOne(context.Background(), payment)
	if err != nil {
		return "", err
	}
	return payment.Id, nil
}

// Fetches a `Payment` from the database based on its domain `Id` (not
// the internal datastore `_id`).
func (p *MongoPaymentsRepo) FetchById(id string) (*Payment, error) {
	var payment Payment
	err := p.collection.FindOne(p.ctx, bson.D{{"id", id}}).Decode(&payment)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			return nil, nil
		}
		return nil, err
	}
	return &payment, nil
}

// Fetches a slice of `Payment`s from the database, based on their `OrganisationId`.
func (p *MongoPaymentsRepo) FetchByOrganisationId(id string) ([]Payment, error) {
	cursor, err := p.collection.Find(p.ctx, bson.D{{"organisationid", id}})
	if err != nil {
		return nil, err
	}
	payments := []Payment{}
	for cursor.Next(p.ctx) {
		var payment Payment
		err = cursor.Decode(&payment)
		if err != nil {
			return nil, err
		}
		payments = append(payments, payment)
	}
	return payments, nil
}

// Deletes a `Payment` with the given `id` from the database.
func (p *MongoPaymentsRepo) Delete(id string) error {
	_, err := p.collection.DeleteOne(p.ctx, bson.D{{"id", id}})
	return err
}

// Updates the given `Payment` in the database, based on its `Id`.
func (p *MongoPaymentsRepo) Update(payment Payment) error {
	_, err := p.collection.UpdateOne(p.ctx, bson.D{{"id", payment.Id}}, bson.D{{"$set", payment}})
	return err
}
