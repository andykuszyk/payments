package main

import (
	"log"
	"net/http"

	"gitlab.com/andykuszyk/payments/api"
)

func main() {
	paymentsRepo, err := api.NewMongoPaymentsRepo()
	if err != nil {
		log.Fatalf("Error created mongo repo: %v", err)
	}
	paymentsApi := api.NewApi(paymentsRepo)
	paymentsApi.AddHandlers()

	server := &http.Server{
		Handler: paymentsApi.Handler,
		Addr:    ":8080",
	}
	server.ListenAndServe()
}
