#!/bin/bash
mongo_container=$(docker run -d -p 27017:27017 mongo:4)
go test ./... -v
docker kill $mongo_container
