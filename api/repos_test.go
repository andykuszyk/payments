package api

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func TestInsert_ShouldInsertToDb(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test that requires database - run with ./unit-test.sh instead.")
	}
	sut, err := NewMongoPaymentsRepo()

	id, err := sut.Insert(paymentModel)

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	assert.NoError(t, err)
	collection := client.Database("payments").Collection("payments")
	var actual Payment
	err = collection.FindOne(ctx, bson.D{{"id", id}}).Decode(&actual)
	assert.NoError(t, err)
	paymentModel.Id = id
	assert.Equal(t, paymentModel, actual)
}

func TestFetchById_ShouldReturnPayment_FollowingInsert(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test that requires database - run with ./unit-test.sh instead.")
	}
	sut, err := NewMongoPaymentsRepo()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	assert.NoError(t, err)
	collection := client.Database("payments").Collection("payments")
	_, err = collection.InsertOne(context.Background(), paymentModel)
	assert.NoError(t, err)

	actual, err := sut.FetchById(paymentModel.Id)

	assert.NoError(t, err)
	assert.Equal(t, &paymentModel, actual)
}

func TestFetchByOrganisationId_ShouldReturnPayments_FollowingInserts(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test that requires database - run with ./unit-test.sh instead.")
	}
	sut, err := NewMongoPaymentsRepo()
	payment1 := paymentModel
	payment1.OrganisationId = "spam-123"
	payment2 := paymentModel
	payment2.OrganisationId = payment1.OrganisationId
	payment1.Id, _ = sut.Insert(payment1)
	payment2.Id, _ = sut.Insert(payment2)

	payments, err := sut.FetchByOrganisationId(payment1.OrganisationId)

	assert.NoError(t, err)
	assert.Equal(t, []Payment{payment1, payment2}, payments)
}

func TestDelete_ShouldRemoveFromDatastore(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test that requires database - run with ./unit-test.sh instead.")
	}
	sut, _ := NewMongoPaymentsRepo()
	id, _ := sut.Insert(paymentModel)

	sut.Delete(id)

	payment, _ := sut.FetchById(id)

	assert.Nil(t, payment)
}

func TestUpdate_ShouldPersistTheChangesMade(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping test that requires database - run with ./unit-test.sh instead.")
	}
	sut, _ := NewMongoPaymentsRepo()
	id, _ := sut.Insert(paymentModel)
	paymentModel.Id = id
	paymentModel.OrganisationId = "spam-eggs"

	sut.Update(paymentModel)
	updatedPayment, _ := sut.FetchById(paymentModel.Id)

	assert.Equal(t, &paymentModel, updatedPayment)
}
