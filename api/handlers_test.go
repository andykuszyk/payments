package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPaymentsPostHandler_ReturnsModelWithId(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodPost, "/payments", strings.NewReader(paymentJson))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPostHandler(w, req)

	var actual, expected Payment
	json.NewDecoder(w.Body).Decode(&actual)
	json.Unmarshal([]byte(paymentJson), &expected)
	expected.Id = actual.Id
	assert.Equal(t, expected, actual)
}

func TestPaymentsPostHandler_Returns400_WhenMalformedRequest(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode("")
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPost, "/payments", &buffer)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPostHandler(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code, "A bad request should be returned if the body is invalid")
}

func TestPaymentsPostHandler_Returns201_WhenInserted(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{Id: "bar"})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPost, "/payments", &buffer)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPostHandler(w, req)

	assert.Equal(t, http.StatusCreated, w.Code, "A created should be returned if the body is valid and was saved")
}

func TestPaymentsPostHandler_Returns503_WhenDbUnavailable(t *testing.T) {
	insertFunc := func(p Payment) (string, error) {
		return "", errors.New("An error occured")
	}

	sut := NewApi(&paymentsRepoMock{insertFunc: insertFunc})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{Id: "bar"})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPost, "/payments", &buffer)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPostHandler(w, req)

	assert.Equal(t, http.StatusServiceUnavailable, w.Code, "503 should be returned if the database is unavailable")
}

func TestPaymentsPutHandler_ReturnsModel(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodPut, "/payments/123-abc", strings.NewReader(paymentJson))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "PUT should return 200 when successful")
	var actual, expected Payment
	json.NewDecoder(w.Body).Decode(&actual)
	json.Unmarshal([]byte(paymentJson), &expected)
	assert.Equal(t, expected, actual)
}

func TestPaymentsPutHandler_Returns200_IfUpdated(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{Id: "bar"})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPut, "/payments/123-abc-456-def", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned when successfully updating a payment")
}

func TestPaymentsPutHandler_ShouldTryUpdatingModel(t *testing.T) {
	updateFuncCallCount := 0
	updateFunc := func(payment Payment) error {
		updateFuncCallCount += 1
		return nil
	}
	sut := NewApi(&paymentsRepoMock{updateFunc: updateFunc})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{Id: "bar"})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPut, "/payments/123-abc-456-def", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, 1, updateFuncCallCount, "The Update function should be called by the PUT route")
}

func TestPaymentsPutHandler_Returns503_WhenUpdateFails(t *testing.T) {
	updateFunc := func(payment Payment) error {
		return errors.New("An error occured")
	}
	sut := NewApi(&paymentsRepoMock{updateFunc: updateFunc})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{Id: "bar"})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPut, "/payments/123-abc-456-def", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, http.StatusServiceUnavailable, w.Code, "A 503 should be returned when the update fails")
}

func TestPaymentsPutHandler_Returns400_IfMalformedBody(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode("")
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPut, "/payments/123-abc-456-def", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code, "A 400 should be returned when the body is malformed")
}

func TestPaymentsPutHandler_Returns400_IfMalformedUrl(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPut, "/payments/", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code, "A 400 should be returned when the body is malformed")
}

func TestPaymentsPutHandler_Returns404_IfPaymentNotFound(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return nil, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPut, "/payments/123-abc-456-def", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code, "A 404 should be returned when the Payment does not exist")
}

func TestGetPaymentId_ShouldReturnIdFromPath(t *testing.T) {
	actual, err := getPaymentId("/payments/foobar")

	assert.Nil(t, err, "No error should be returned matching the path")
	assert.Equal(t, "foobar", actual, "The id should have been matched from the path")
}

func TestPaymentsPutHandler_Returns503_IfDbUnavailable(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return nil, errors.New("Db connection failed")
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodPut, "/payments/123-abc-456-def", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsPutHandler(w, req)

	assert.Equal(t, http.StatusServiceUnavailable, w.Code, "A 503 should be returned if the db is unavailable")
}

func TestPaymentsDeleteHandler_Returns200_IfDeleteSuccessful(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	var buffer bytes.Buffer
	err := json.NewEncoder(&buffer).Encode(Payment{})
	if err != nil {
		assert.Error(t, err)
	}
	req, err := http.NewRequest(http.MethodDelete, "/payments/123-abc-456-def", &buffer)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsDeleteHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned when the Payment is deleted successfully")
}

func TestPaymentsDeleteHandler_Returns404_IfPaymentNotFound(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return nil, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodDelete, "/payments/123-abc-456-def", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsDeleteHandler(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code, "A 404 should be returned when the Payment is not in the db")
}

func TestPaymentsDeleteHandler_Returns400_IfUrlMalformed(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodDelete, "/payments/", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsDeleteHandler(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code, "A 400 should be returned if the url is malformed")
}

func TestPaymentsDeleteHandler_Returns503_WhenDbUnavailableToFetch(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return nil, errors.New("Db connection failed")
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodDelete, "/payments/abc-123", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsDeleteHandler(w, req)

	assert.Equal(t, http.StatusServiceUnavailable, w.Code, "A 503 should be returned when an error occurs fetching the payment")
}

func TestPaymentsDeleteHandler_Returns503_WhenDbUnavailableToDelete(t *testing.T) {
	deleteFunc := func(id string) error {
		return errors.New("An error occured")
	}
	sut := NewApi(&paymentsRepoMock{deleteFunc: deleteFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodDelete, "/payments/abc-123", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsDeleteHandler(w, req)

	assert.Equal(t, http.StatusServiceUnavailable, w.Code, "A 503 should be returned when an error occurs deleting the payment")
}

func TestPaymentsGetHandler_ReturnsModel(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		var payment Payment
		json.Unmarshal([]byte(paymentJson), &payment)
		return &payment, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments/123-abc", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "GET should return 200 when successful")
	var actual, expected Payment
	json.NewDecoder(w.Body).Decode(&actual)
	json.Unmarshal([]byte(paymentJson), &expected)
	assert.Equal(t, expected, actual)
}

func TestPaymentsGetHandler_NoParameters_Returns200_WhenSuccessful(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments/abc-123", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned when the payment is gotten successfully")
}

func TestPaymentsGetHandler_NoParameters_Returns404_WhenPaymentNotFound(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return nil, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments/abc-123", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code, "A 404 should be returned when the payment is not in the datastore")
}

func TestPaymentsGetHandler_NoParameters_Returns400_WhenUrlMalformed(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments/", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code, "A 400 should be returned when the url is malformed")
}

func TestPaymentsGetHandler_NoParameters_Returns503_WhenDbUnavailable(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return nil, errors.New("An error occured")
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments/abc-123", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusServiceUnavailable, w.Code, "A 503 should be returned when the database is unavailable")
}

func TestPaymentsGetHandler_OrganisationParameter_Returns200WhenSuccessful(t *testing.T) {
	fetchByOrganisationIdFunc := func(id string) ([]Payment, error) {
		return []Payment{Payment{}}, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByOrganisationIdFunc: fetchByOrganisationIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments?organisationId=cde-456", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned when successfully getting by organisation id")
}

func TestPaymentsGetHandler_WithOrganisationIdParameter_ReturnsModel(t *testing.T) {
	fetchByOrganisationIdFunc := func(id string) ([]Payment, error) {
		var payment Payment
		json.Unmarshal([]byte(paymentJson), &payment)
		return []Payment{payment, payment}, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByOrganisationIdFunc: fetchByOrganisationIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments?organisationId=123-abc", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "GET should return 200 when successful")
	var actual, expected []Payment
	json.NewDecoder(w.Body).Decode(&actual)
	paymentsListJson := fmt.Sprintf("[%s, %s]", paymentJson, paymentJson)
	json.Unmarshal([]byte(paymentsListJson), &expected)
	assert.Equal(t, expected, actual)
}

func TestPaymentsGetHandler_OrganisationParameter_Returns404WhenNoPaymentsFound(t *testing.T) {
	fetchByOrganisationIdFunc := func(id string) ([]Payment, error) {
		return []Payment{}, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByOrganisationIdFunc: fetchByOrganisationIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments?organisationId=cde-456", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code, "A 404 should be returned when no payments could be found for the organisation id")
}

func TestPaymentsGetHandler_UnknownParameter_Returns400(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments?foo=bar", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code, "A 400 should be returned when an unknown parameter is used")
}

func TestPaymentsGetHandler_OrganisationParameter_Returns503WhenDbUnavailable(t *testing.T) {
	fetchByOrganisationIdFunc := func(id string) ([]Payment, error) {
		return nil, errors.New("An error occured")
	}
	sut := NewApi(&paymentsRepoMock{fetchByOrganisationIdFunc: fetchByOrganisationIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments?organisationId=cde-456", nil)
	if err != nil {
		assert.Error(t, err)
	}
	w := httptest.NewRecorder()

	sut.paymentsGetHandler(w, req)

	assert.Equal(t, http.StatusServiceUnavailable, w.Code, "A 503 should be returned when the db returns an error")
}
