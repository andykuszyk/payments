package api

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Api struct {
	paymentsRepo PaymentsRepo
	Handler      *mux.Router
}

func (a *Api) AddHandlers() {
	a.Handler.HandleFunc("/payments", a.paymentsPostHandler).Methods(http.MethodPost)
	a.Handler.HandleFunc("/payments/{id}", a.paymentsGetHandler).Methods(http.MethodGet)
	a.Handler.HandleFunc("/payments", a.paymentsGetHandler).Methods(http.MethodGet)
	a.Handler.HandleFunc("/payments/{id}", a.paymentsPutHandler).Methods(http.MethodPut)
	a.Handler.HandleFunc("/payments/{id}", a.paymentsDeleteHandler).Methods(http.MethodDelete)
}

func NewApi(paymentsRepo PaymentsRepo) Api {
	return Api{
		paymentsRepo: paymentsRepo,
		Handler:      mux.NewRouter(),
	}
}
