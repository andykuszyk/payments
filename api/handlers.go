package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"regexp"
)

// Handles POST /payments requests.
func (a *Api) paymentsPostHandler(w http.ResponseWriter, r *http.Request) {
	payment := Payment{}
	err := json.NewDecoder(r.Body).Decode(&payment)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	id, err := a.paymentsRepo.Insert(payment)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	payment.Id = id
	bytes, err := json.Marshal(&payment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(bytes)
}

// Attempts to find the id from a URL path of the form `/payments/:id`, returning
// an error if this pattern could not be matched.
var getPaymentId = getPaymentIdFunc()

// Builds the `getPaymentId` function, for matching payment ids from urls of the form `/payments/:id`.
func getPaymentIdFunc() func(p string) (string, error) {
	paymentsIdRegexp, err := regexp.Compile("/payments/(.+)/*") // The regexp for matching ids from the form `/payments/:id`
	if err != nil {
		log.Fatalf("Error building regexp for matching payment ids: %v", err)
	}
	return func(p string) (string, error) {
		submatches := paymentsIdRegexp.FindStringSubmatch(p)
		if len(submatches) < 2 {
			return "", errors.New("The id could not be matched")
		}
		return submatches[1], nil
	}
}

// Handlers PUT /payments/:id requests.
func (a *Api) paymentsPutHandler(w http.ResponseWriter, r *http.Request) {
	payment := Payment{}
	err := json.NewDecoder(r.Body).Decode(&payment)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	paymentId, err := getPaymentId(r.URL.Path)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	p, err := a.paymentsRepo.FetchById(paymentId)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	if p == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	err = a.paymentsRepo.Update(payment)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	bytes, err := json.Marshal(&payment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(bytes)
}

// Handles DELETE /payments/:id requests.
func (a *Api) paymentsDeleteHandler(w http.ResponseWriter, r *http.Request) {
	paymentId, err := getPaymentId(r.URL.Path)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	p, err := a.paymentsRepo.FetchById(paymentId)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	if p == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	err = a.paymentsRepo.Delete(paymentId)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// Handlers GET /payments/:id and /payments/:id?param=value requests.
func (a *Api) paymentsGetHandler(w http.ResponseWriter, r *http.Request) {
	parameters := r.URL.Query()
	if len(parameters) == 0 {
		paymentId, err := getPaymentId(r.URL.Path)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		payment, err := a.paymentsRepo.FetchById(paymentId)
		if err != nil {
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}
		if payment == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		bytes, err := json.Marshal(&payment)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(bytes)
	} else {
		if parameters["organisationId"] != nil {
			payments, err := a.paymentsRepo.FetchByOrganisationId(parameters["organisationId"][0])
			if err != nil {
				w.WriteHeader(http.StatusServiceUnavailable)
				return
			}
			if len(payments) == 0 {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			bytes, err := json.Marshal(&payments)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Write(bytes)
		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	}
	w.WriteHeader(http.StatusOK)
}
