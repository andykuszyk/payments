package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// A mock for the `PaymentsRepo` interface.
type paymentsRepoMock struct {
	insertFunc                func(p Payment) (string, error)    // Allows an implementation to be set for the `Insert` method.
	fetchByIdFunc             func(id string) (*Payment, error)  // Allows an implementation to be set for the `FetchById` method.
	fetchByOrganisationIdFunc func(id string) ([]Payment, error) // Allows an implementation to be set for the `FetchByOrganisationId` method.
	deleteFunc                func(id string) error              // Allows an implementation to be set for the `Delete` method.
	updateFunc                func(p Payment) error              // Allows an implementation to be set for the `Update` method.
}

func (p *paymentsRepoMock) Insert(payment Payment) (string, error) {
	if p.insertFunc == nil {
		return "", nil
	}
	return p.insertFunc(payment)
}

func (p *paymentsRepoMock) FetchById(id string) (*Payment, error) {
	if p.fetchByIdFunc == nil {
		return &Payment{}, nil
	}
	return p.fetchByIdFunc(id)
}

func (p *paymentsRepoMock) FetchByOrganisationId(id string) ([]Payment, error) {
	if p.fetchByOrganisationIdFunc == nil {
		return []Payment{}, nil
	}
	return p.fetchByOrganisationIdFunc(id)
}

func (p *paymentsRepoMock) Delete(id string) error {
	if p.deleteFunc == nil {
		return nil
	}
	return p.deleteFunc(id)
}

func (p *paymentsRepoMock) Update(payment Payment) error {
	if p.updateFunc == nil {
		return nil
	}
	return p.updateFunc(payment)
}

func TestApi_PostPayments_ShouldReturnModel(t *testing.T) {
	sut := NewApi(&paymentsRepoMock{})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodPost, "/payments", strings.NewReader(paymentJson))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.Handler.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code, "A 201 should be returned")
	var actual, expected Payment
	json.NewDecoder(w.Body).Decode(&actual)
	json.Unmarshal([]byte(paymentJson), &expected)
	expected.Id = actual.Id
	assert.Equal(t, expected, actual)
}

func TestApi_GetPayments_ShouldReturnModel(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return &paymentModel, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments/abc-123", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.Handler.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned")
	var actual, expected Payment
	json.NewDecoder(w.Body).Decode(&actual)
	json.Unmarshal([]byte(paymentJson), &expected)
	assert.Equal(t, expected, actual)
}

func TestApi_GetPaymentsByOrganisation_ShouldReturnModels(t *testing.T) {
	fetchByOrganisationIdFunc := func(id string) ([]Payment, error) {
		return []Payment{paymentModel, paymentModel}, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByOrganisationIdFunc: fetchByOrganisationIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodGet, "/payments?organisationId=abc-123", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.Handler.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned")
	var actual, expected Payment
	json.NewDecoder(w.Body).Decode(&actual)
	paymentsJson := fmt.Sprintf("[%s, %s]", paymentJson, paymentJson)
	json.Unmarshal([]byte(paymentsJson), &expected)
	assert.Equal(t, expected, actual)
}

func TestApi_PutPayments_ShouldReturnModel(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return &paymentModel, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodPut, "/payments/abc-123", strings.NewReader(paymentJson))
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.Handler.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned")
	var actual, expected Payment
	json.NewDecoder(w.Body).Decode(&actual)
	json.Unmarshal([]byte(paymentJson), &expected)
	assert.Equal(t, expected, actual)
}

func TestApi_DeletePayments_ShouldReturn200(t *testing.T) {
	fetchByIdFunc := func(id string) (*Payment, error) {
		return &paymentModel, nil
	}
	sut := NewApi(&paymentsRepoMock{fetchByIdFunc: fetchByIdFunc})
	sut.AddHandlers()
	req, err := http.NewRequest(http.MethodDelete, "/payments/abc-123", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()

	sut.Handler.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code, "A 200 should be returned")
}
