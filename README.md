# payments

## Design
The design for this API can be found in the repo wiki, [here](https://gitlab.com/andykuszyk/payments/wikis/Design).

## Running locally
The easiest way to run the API and database locally is by using `docker stack deploy`:

```
docker stack deploy -c docker-compose.yml payments
```

A payment can be created using test data with:

```
curl localhost:8080/payments -d @testdata/payment.json
```

With the id returned by this request, you can retrieve the payment with:

```
curl localhost:8080/payments/:id
```

As well as updating it with:

```
curl localhost:8080/payment/:id -X PUT -d @testdata/payment.json
```

And querying it by organisation id with:

```
curl localhost:8080/payments?organisationId=:organisationId
```

Finally, payments can be deleted with:

```
curl -X DELETE localhost:8080/payments/:id
```

## Running the tests
Some of the unit tests in this project depend on a working MongoDB instance and will fail without it. In order to run these tests safely, use:

```
./unit-test.sh
```

which will start a MongoDB container prior to the test run, and remove it again afterwards.

A suite of integration tests, which test all of the routes on the API, can be run with:

```
./integration-test.sh
```

## Further work
Whilst this API fulfills the requirements laid out in the design document, there are a number of areas where further work could be done:

* Middleware to catch panics and gracefully return a descriptive 500;
* Middleware to handle some kind of authorization scheme - at the moment, all the routes on the API are unprotected from public access;
* The database could be more aware of the data it is housing, especially with respect to monetary values - some other data type could be used for these values, such as an int or an accurate decimal type;
* Middleware for HTTP access logging, as well as domain specific logging throughout the application. This would be useful for log analysis (e.g. in Elasticsearch), but could also be used to expose metrics about the application (e.g. in Prometheus);
* The Mongo database currently lives in an ephemeral container - a volume could be used to persist the data between instances of the database container;
* Whilst the delete route does delete the data in the datastore, it might be more pragmatic to keep the data in some form and mark it as deleted from the application's point of view;
* Expand the simple CI in place at the moment to run the full suite of unit tests and integration tests - this requires the build agent having access to Docker.
